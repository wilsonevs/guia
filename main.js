

// Activación evento modal del archivo index.html
$(function () {
  // Relizar una accion, inmediatmente
  $('#contacto').on('show.bs.modal', function (e) {
    // removeClass: Remueve una clase.
    $('#contactanos-btn').removeClass('btn-outline-success');
    // addClass: Agrega una clase.
    $('#contactanos-btn').addClass('btn-dark');
  });

  // Relizar una accion, cuando se oculta
  $('#contacto').on('hide.bs.modal', function (e) {
    $('#contactanos-btn').removeClass('btn-dark');
    $('#contactanos-btn').addClass('btn-outline-success');
  });

});

$(function () {
  // Activación popover
  $('[data-toggle="popover"]').popover({
    placement: 'bottom',
    trigger: 'hover',
    delay: 300,
    trigger: focus
  });

  console.log("mensaje 0");

  // Activación tooltip  
  $('[data-toggle="tooltip"]').tooltip();
  $('.collapse').collapse({
    toggle: true
  });


  // Activación carousel
  $('.carousel').carousel({
    interval: 2500,
    keyboard: true,
    pause: 'hover'
  });



})