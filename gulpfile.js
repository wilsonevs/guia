const gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create();;


gulp.task('sass', function () {
    gulp.src('./css/*.sass')
        .pipe(sass().on('error', sass.logError))
        
        .pipe(sass({
            outputStyle: 'expended',
            sourceComments:true
        }))
        .pipe(autoprefixer({
            versions: ['last 2 browsers']
        }))
        .pipe(gulp.dest('./css2'));
})

gulp.task('sass:watch', function () {
    gulp.watch('./css/*.sass', ['sass']);
});

gulp.task('browser-sync', function () {
    var files = ['./*.html', './css/*.css', './imagenes/*.{png,jpg,gif,jpng}', './*.js']
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
});

gulp.task('default', ['browser-sync'], function () {
    gulp.star('sass:watch');
});